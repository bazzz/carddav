package main

import (
	"log"
	"net/http"

	"gitlab.com/bazzz/carddav"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r)
	})
	http.Handle("/carddav/", &carddav.Handler{})
	if err := http.ListenAndServe(":80", nil); err != nil {
		log.Fatalln(err)
	}
}
