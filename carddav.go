package carddav

import (
	"fmt"
	"log"
	"net/http"

	"github.com/beevik/etree"
)

type Handler struct {
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for key, value := range r.Header {
		fmt.Println(key, value)
	}
	/*
		defer r.Body.Close()
		data, err := io.ReadAll(r.Body)
		if err != nil {
			log.Println("could not read request body:", err)
			return
		}
		fmt.Println("METHOD:", r.Method)
		fmt.Println("BODY:")
		fmt.Println(string(data))
	*/
	switch r.Method {
	case "PROPFIND":
		propFind(w, r)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func propFind(w http.ResponseWriter, r *http.Request) {
	requestXML := etree.NewDocument()
	if _, err := requestXML.ReadFrom(r.Body); err != nil {
		log.Println("could not read request xml:", err)
		return
	}
	if requestXML.Root().Tag != "propfind" {
		log.Println("request xml is not propfind")
		return
	}
	responseXML := etree.NewDocument()
	root := responseXML.CreateElement("multistatus")
	root.Attr = append(root.Attr, etree.Attr{Key: "D", Space: "xmlns", Value: "DAV:"})
	root.Attr = append(root.Attr, etree.Attr{Key: "C", Space: "xmlns", Value: "urn:ietf:params:xml:ns:carddav"})

	resp := root.CreateElement("response")
	for _, element := range requestXML.FindElements("/propfind/prop/*") {
		switch element.Tag {
		case "resourcetype":
			propstat := resp.CreateElement("D:propstat")
			status := propstat.CreateElement("D:status")
			status.CreateText("HTTP/1.1 200 OK")
			prop := propstat.CreateElement("D:prop")
			resourcetype := prop.CreateElement("D:" + element.Tag)
			resourcetype.CreateElement("D:collection")
			resourcetype.CreateElement("C:addressbook")
		case "displayname":
			propstat := resp.CreateElement("D:propstat")
			status := propstat.CreateElement("D:status")
			status.CreateText("HTTP/1.1 200 OK")
			prop := propstat.CreateElement("D:prop")
			resourcetype := prop.CreateElement("D:" + element.Tag)
			resourcetype.CreateText("Mijn boekje")
		case "current-user-principal":
			propstat := resp.CreateElement("D:propstat")
			status := propstat.CreateElement("D:status")
			status.CreateText("HTTP/1.1 200 OK")
			prop := propstat.CreateElement("D:prop")
			principle := prop.CreateElement("D:" + element.Tag)
			href := principle.CreateElement("D:href")
			href.CreateText("/carddav/bas/")
		case "current-user-privilege-set":
			propstat := resp.CreateElement("D:propstat")
			prop := propstat.CreateElement("D:prop")
			status := propstat.CreateElement("D:status")
			status.CreateText("HTTP/1.1 200 OK")
			privilegeSet := prop.CreateElement("D:" + element.Tag)
			privilege := privilegeSet.CreateElement("D:privilege")
			privilege.CreateElement("D:all")
		default:
			propstat := resp.CreateElement("D:propstat")
			prop := propstat.CreateElement("D:prop")
			prop.CreateElement(element.FullTag())
			status := propstat.CreateElement("D:status")
			status.CreateText("HTTP/1.1 404 Not Found")
		}
	}
	w.Header().Add("Content-Type", "text/xml")
	w.WriteHeader(http.StatusMultiStatus)
	responseXML.WriteTo(w)
}
